def is_triangle(a, b, c):
    
    if a + b <= c:
        return False
    elif a + c <= b:
        return False
    elif c + b <= a:
        return False
    else:
        return True
